<?php
/**
 * Plugin Name: Mentecontorta Customizations
 * Description: Customizations for Mentecontorta's theme
 * Version: 0.1
 * Author: Giustino Borzacchiello
 */

function mentecontorta_load_plugin_textdomain() {
	load_plugin_textdomain( 'mentecontorta', false, basename( dirname( __FILE__ ) ) . '/languages/' );
}

add_action( 'plugins_loaded', 'mentecontorta_load_plugin_textdomain' );


if ( ! function_exists( 'ingredient_taxonomy' ) ) {

// Register Custom Taxonomy
	function ingredient_taxonomy() {

		$labels = array(
			'name'                       => _x( 'Ingredients', 'Taxonomy General Name', 'mentecontorta' ),
			'singular_name'              => _x( 'Ingredient', 'Taxonomy Singular Name', 'mentecontorta' ),
			'menu_name'                  => __( 'Ingredient', 'mentecontorta' ),
			'all_items'                  => __( 'All Ingredients', 'mentecontorta' ),
			'parent_item'                => __( 'Parent Ingredient', 'mentecontorta' ),
			'parent_item_colon'          => __( 'Parent Ingredient:', 'mentecontorta' ),
			'new_item_name'              => __( 'New Ingredient Name', 'mentecontorta' ),
			'add_new_item'               => __( 'Add New Ingredient', 'mentecontorta' ),
			'edit_item'                  => __( 'Edit Ingredient', 'mentecontorta' ),
			'update_item'                => __( 'Update Ingredient', 'mentecontorta' ),
			'separate_items_with_commas' => __( 'Separate ingredients with commas', 'mentecontorta' ),
			'search_items'               => __( 'Search ingredients', 'mentecontorta' ),
			'add_or_remove_items'        => __( 'Add or remove ingredients', 'mentecontorta' ),
			'choose_from_most_used'      => __( 'Choose from the most used ingredients', 'mentecontorta' ),
			'not_found'                  => __( 'Not Found', 'mentecontorta' ),
		);
		$args   = array(
			'labels'            => $labels,
			'hierarchical'      => false,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
		);
		register_taxonomy( 'ingredient', array( 'post' ), $args );

	}

// Hook into the 'init' action
	add_action( 'init', 'ingredient_taxonomy', 0 );

}

// Register Custom Taxonomy
function season_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Seasons', 'Taxonomy General Name', 'mentecontorta' ),
		'singular_name'              => _x( 'Season', 'Taxonomy Singular Name', 'mentecontorta' ),
		'menu_name'                  => __( 'Season', 'mentecontorta' ),
		'all_items'                  => __( 'All Seasons', 'mentecontorta' ),
		'parent_item'                => __( 'Parent Season', 'mentecontorta' ),
		'parent_item_colon'          => __( 'Parent Season:', 'mentecontorta' ),
		'new_item_name'              => __( 'New Season Name', 'mentecontorta' ),
		'add_new_item'               => __( 'Add New Season', 'mentecontorta' ),
		'edit_item'                  => __( 'Edit Season', 'mentecontorta' ),
		'update_item'                => __( 'Update Season', 'mentecontorta' ),
		'separate_items_with_commas' => __( 'Separate seasons with commas', 'mentecontorta' ),
		'search_items'               => __( 'Search seasons', 'mentecontorta' ),
		'add_or_remove_items'        => __( 'Add or remove seasons', 'mentecontorta' ),
		'choose_from_most_used'      => __( 'Choose from the most used seasons', 'mentecontorta' ),
		'not_found'                  => __( 'Not Found', 'mentecontorta' ),
	);
	$args   = array(
		'labels'            => $labels,
		'hierarchical'      => true,
		'public'            => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud'     => true,
	);
	register_taxonomy( 'season', array( 'post' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'season_taxonomy', 0 );


// Register Custom Taxonomy
function meal_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Meals', 'Taxonomy General Name', 'mentecontorta' ),
		'singular_name'              => _x( 'Meal', 'Taxonomy Singular Name', 'mentecontorta' ),
		'menu_name'                  => __( 'Meal', 'mentecontorta' ),
		'all_items'                  => __( 'All Meals', 'mentecontorta' ),
		'parent_item'                => __( 'Parent Meal', 'mentecontorta' ),
		'parent_item_colon'          => __( 'Parent Meal:', 'mentecontorta' ),
		'new_item_name'              => __( 'New Meal Name', 'mentecontorta' ),
		'add_new_item'               => __( 'Add New Meal', 'mentecontorta' ),
		'edit_item'                  => __( 'Edit Meal', 'mentecontorta' ),
		'update_item'                => __( 'Update Meal', 'mentecontorta' ),
		'separate_items_with_commas' => __( 'Separate meals with commas', 'mentecontorta' ),
		'search_items'               => __( 'Search meals', 'mentecontorta' ),
		'add_or_remove_items'        => __( 'Add or remove meals', 'mentecontorta' ),
		'choose_from_most_used'      => __( 'Choose from the most used meals', 'mentecontorta' ),
		'not_found'                  => __( 'Not Found', 'mentecontorta' ),
	);
	$args   = array(
		'labels'            => $labels,
		'hierarchical'      => true,
		'public'            => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud'     => true,
	);
	register_taxonomy( 'meal', array( 'post' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'meal_taxonomy', 0 );

// Register Custom Taxonomy
function course_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Courses', 'Taxonomy General Name', 'mentecontorta' ),
		'singular_name'              => _x( 'Course', 'Taxonomy Singular Name', 'mentecontorta' ),
		'menu_name'                  => __( 'Course', 'mentecontorta' ),
		'all_items'                  => __( 'All Courses', 'mentecontorta' ),
		'parent_item'                => __( 'Parent Courses', 'mentecontorta' ),
		'parent_item_colon'          => __( 'Parent Courses:', 'mentecontorta' ),
		'new_item_name'              => __( 'New Course Name', 'mentecontorta' ),
		'add_new_item'               => __( 'Add New Course', 'mentecontorta' ),
		'edit_item'                  => __( 'Edit Course', 'mentecontorta' ),
		'update_item'                => __( 'Update Course', 'mentecontorta' ),
		'separate_items_with_commas' => __( 'Separate courses with commas', 'mentecontorta' ),
		'search_items'               => __( 'Search courses', 'mentecontorta' ),
		'add_or_remove_items'        => __( 'Add or remove courses', 'mentecontorta' ),
		'choose_from_most_used'      => __( 'Choose from the most used courses', 'mentecontorta' ),
		'not_found'                  => __( 'Not Found', 'mentecontorta' ),
	);
	$args   = array(
		'labels'            => $labels,
		'hierarchical'      => true,
		'public'            => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud'     => true,
	);
	register_taxonomy( 'course', array( 'post' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'course_taxonomy', 0 );

add_action('wp_head', function(){
	?>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5HCH6L5');</script>
	<!-- End Google Tag Manager -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-52557387-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-52557387-2');
</script>
<!-- MailerLite Universal -->
<script>
(function(m,a,i,l,e,r){ m['MailerLiteObject']=e;function f(){
var c={ a:arguments,q:[]};var r=this.push(c);return "number"!=typeof r?r:f.bind(c.q);}
f.q=f.q||[];m[e]=m[e]||f.bind(f.q);m[e].q=m[e].q||f.q;r=a.createElement(i);
var _=a.getElementsByTagName(i)[0];r.async=1;r.src=l+'?'+(~~(new Date().getTime()/10000000));
_.parentNode.insertBefore(r,_);})(window, document, 'script', 'https://static.mailerlite.com/js/universal.js', 'ml');

var ml_account = ml('accounts', '643351', 'k1g0p0e5x9', 'load');
</script>
<!-- End MailerLite Universal -->
<!-- Hotjar Tracking Code for www.mentecontorta.it -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:346754,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
	<?php

	/*
	<!-- Start Google Ads -->
<!-- <script data-ad-client="ca-pub-5756040520863703" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> -->
<!-- End Google Ads -->
*/
});

add_action('wp_body_open', function(){
	?>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HCH6L5"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php
});

//function myfeed_request( $qv ) {
//	if ( isset( $qv['feed'] ) && ! isset( $qv['post_type'] ) ) {
//		$qv['post_type'] = array( 'post', 'recipe' );
//	}
//
//	return $qv;
//}
//
//add_filter( 'request', 'myfeed_request' );
//
//function rss_post_thumbnail( $content ) {
//	global $post;
//	if ( has_post_thumbnail( $post->ID ) ) {
//		$content = '<p>' . get_the_post_thumbnail( $post->ID, 'medium' ) .
//		           '</p>' . $content;
//	}
//
//	return $content;
//}
//
//add_filter( 'the_excerpt_rss', 'rss_post_thumbnail' );
//add_filter( 'the_content_feed', 'rss_post_thumbnail' );
